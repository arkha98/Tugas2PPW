"""Tugas_2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import app_1.urls as app_1
import app_2.urls as app_2
import app_4.urls as app_4
import app_3.urls as app_3
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^app-1/', include(app_1, namespace='app-1')),
    url(r'^app-2/', include(app_2, namespace='app-2')),
    url(r'^app-4/', include(app_4, namespace='app-4')),
    url(r'^app-3/', include(app_3, namespace='app-3')),
]
