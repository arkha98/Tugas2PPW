# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Keahlian(models.Model):
	npm = models.CharField(max_length=10)
	ahli = models.CharField(max_length=255)
	level = models.CharField(max_length=12)
	email = models.CharField(max_length=100)
	profil = models.CharField(max_length=50)