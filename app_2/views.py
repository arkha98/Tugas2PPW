# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render
from app_1.models import Mahasiswa, Status
from .models import Keahlian
from django.urls import reverse
from app_1.models import Mahasiswa

# Create your views here.
response = {}

def index(request):
	html = 'app_2/app_2.html'
	response['nama'] = request.session['user_login']
	response['npm'] = request.session['kode_identitas']
	response['jumlah_status'] = Status.objects.all().count()
	response['status_baru'] = Status.objects[0]
	response['email'] = Keahlian.objects.filter(npm=npm).email
	response['keahlian'] = Keahlian.objects.filter(npm=npm).ahli
	response['profil'] = Keahlian.objects.filter(npm = npm).profil
	return render(request, html, response)

def edit(request):
	html = 'app_2/edit.html'

	return render(request, html, response)