from django.conf.urls import url
from .views import index, edit

urlpatterns = [
	url(r'^profile/', index, name='index'),
	url(r'^profile/edit', edit, name='edit'),
	# url()
]