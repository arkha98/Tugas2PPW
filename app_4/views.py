from django.shortcuts import render
#from .models import Mahasiswa, Keahlian
from app_1.models import Mahasiswa
from app_2.models import Keahlian

def index(request):
    mahasiswa_list = Mahasiswa.objects.all()

    #Mahasiswa.objects.create(name="Eddie Wayne", npm="1306937591", angkatan="2013")
    #Mahasiswa.objects.create(name="Ford Allen", npm="0906120472", angkatan="2009")
    #Mahasiswa.objects.create(name="Mary Laird", npm="1006338013", angkatan="2010")
    #Mahasiswa.objects.create(name="Charles Alan", npm="0806733874", angkatan="2008")
    #Mahasiswa.objects.create(name="Frank Finn", npm="1206397735", angkatan="2012")
    #Mahasiswa.objects.create(name="Terry Mack", npm="1106844517", angkatan="2011")
    #Mahasiswa.objects.create(name="Ann Claire", npm="0706883958", angkatan="2007")
    #Mahasiswa.objects.create(name="Jimmie Dale", npm="0906822739", angkatan="2009")
    #Mahasiswa.objects.create(name="Emma Dale", npm="1306733210", angkatan="2013")
    #Mahasiswa.objects.create(name="David Ryan", npm="1106299411", angkatan="2011")
    #Mahasiswa.objects.create(name="Tressa Gail", npm="0806235512", angkatan="2008")
    #Mahasiswa.objects.create(name="Charlie Clyde", npm="1006235513", angkatan="2010")
    #Mahasiswa.objects.create(name="Zack Alan", npm="1006234414", angkatan="2010")
    #Mahasiswa.objects.create(name="Sarah Michelle", npm="0906348815", angkatan="2009")
    #Mahasiswa.objects.create(name="Carol Kaye", npm="1106937716", angkatan="2011")
    #Mahasiswa.objects.create(name="Don Allan", npm="0806499517", angkatan="2008")
    #Mahasiswa.objects.create(name="John Preston", npm="1106093318", angkatan="2011")
    #Mahasiswa.objects.create(name="Betty Grace", npm="0906722619", angkatan="2009")
    #Mahasiswa.objects.create(name="Jack Carl", npm="1206899520", angkatan="2012")

    html = 'app_4/cari_mahasiswa.html'
    response = {}
    response['mahasiswa_list'] = mahasiswa_list
    return render(request, html, response)