from django.db import models

class Mahasiswa(models.Model):
	name = models.CharField(max_length=30)
	npm = models.CharField(max_length=10)
	angkatan = models.CharField(max_length=4)

class Keahlian(models.Model):
	keahlian = models.CharField(max_length=255)
	level = models.CharField(max_length=12)