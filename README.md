[![build status](https://gitlab.com/arkha98/Tugas2PPW/badges/master/build.svg)](https://gitlab.com/arkha98/Tugas2PPW/commits/master) 
[![coverage report](https://gitlab.com/arkha98/Tugas2PPW/badges/master/coverage.svg)](https://gitlab.com/arkha98/Tugas2PPW/commits/master)

# Tugas 2 PPW

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## Anggota Kelompok

- Arkha Sayoga Mayadi
- Fari Qodri Andana
- Hema Mitta Kalyani
- Rico Putra Pradana

## Heroku App Link

https://ppw-lingin-v2.herokuapp.com 