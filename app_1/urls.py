from django.conf.urls import url
from .views import index, login_sso, update_status, add_status
from .custom_auth import auth_login, auth_logout

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^auth-login/', auth_login, name='auth_login'),
	url(r'^auth-logout/', auth_logout, name='auth_logout'),
	url(r'^login-sso/', login_sso, name='login_sso'),
	url(r'^update-status/', update_status, name='update_status'),
	url(r'^add-status/', add_status, name='add_status'),
]