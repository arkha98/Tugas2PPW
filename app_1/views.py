from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import Status_Form
# Create your views here.
response = {}

def index(request):
	if 'user_login' in request.session:
		return HttpResponseRedirect(reverse('app-2:profile'))
	else:
		html = 'app_1/landing-page.html'
		return render(request, html, response)

def login_sso(request):
	if 'user_login' in request.session:
		return HttpResponseRedirect(reverse('app-2:profile'))
	else:
		html = 'app_1/login.html'
		return render(request, html, response)

def update_status(request):
	if 'user_login' in request.session:
		html = 'app_1/update_status.html'
		return render(request, html, response)
	else:
		html = 'app_1/landing-page.html'
		return render(request, html, response)

def add_status(request):
	form = Status_Form(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		response['description'] = request.POST['description']
		npm = get_data_user(request, 'kode_identitas')
		mahasiswa = Mahasiswa.objects.get(npm=npm)
		status = Status(mahasiswa=mahasiswa, description=response['description'])
		status.save()
		all_status = Status.objects.all()
		response['all_status'] = all_status
		html = 'app_1/update-status.html'
		return HttpResponseRedirect(reverse('app-1:update_status'))
	else:
		return HttpResponseRedirect(reverse('app-1:update_status'))

def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data
