from django.db import models

# Create your models here.
class Mahasiswa(models.Model):
	name = models.CharField(max_length=30)
	npm = models.CharField(max_length=10, primary_key=True, unique=True)
	angkatan = models.CharField(max_length=4)

class Status(models.Model):
	mahasiswa = models.ForeignKey(Mahasiswa)
	description = models.CharField(max_length=280)
	created_date = models.DateTimeField(auto_now_add=True)
