# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 22:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_1', '0004_auto_20171213_0516'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mahasiswa',
            name='npm',
            field=models.CharField(max_length=10, primary_key=True, serialize=False, unique=True),
        ),
    ]
